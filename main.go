package main

import (
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-sms/messaging"
)

func main() {
	tsatsubii.Init()
	messaging.Setup()
	tsatsubii.Start()
}
