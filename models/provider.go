package models

import (
	"gitlab.com/tsatsubii/tsatsubii-service"
)

type ProviderStatus string

const (
	ProviderActive   ProviderStatus = "active"
	ProviderInactive ProviderStatus = "inactive"
)

type Provider struct {
	tsatsubii.Model
	Name     string         `gorm:"unique" json:"name"`
	Provider string         `json:"provider"`
	Order    int            `gorm:"unique" json:"order"`
	Status   ProviderStatus `json:"status"`
}
