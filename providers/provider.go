package providers

type ISmsProvider interface {
	Balance() int
	Send(to string, msg string) bool
}
