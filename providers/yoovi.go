package providers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

const (
	yooviBaseURL string = "http://pipe.yoovi.me/sms/api"
)

type YooviBalanceResponse struct {
	Balance int    `json:"balance"`
	Country string `json:"country"`
	User    string `json:"user"`
}

type YooviSendResponse struct {
	Balance     int     `json:"balance"`
	Code        string  `json:"code"`
	MainBalance float64 `json:main_balance"`
	Message     string  `json:"message"`
	User        string  `json:"user"`
}

type Yoovi struct {
	ApiKey   string
	SenderID string
}

func NewYoovi(apiKey string, senderID string) Yoovi {
	return Yoovi{
		ApiKey:   apiKey,
		SenderID: senderID,
	}
}

func (y *Yoovi) Send(to string, msg string) bool {
	var result YooviSendResponse

	url := fmt.Sprintf(
		"%s?action=send-sms&api_key=%s&to=%s&from=%s&sms=%s",
		yooviBaseURL,
		y.ApiKey,
		to,
		y.SenderID,
		url.QueryEscape(msg),
	)

	resp, err := http.Get(url)
	if err != nil {
		return false
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return false
	}

	return result.Code == "ok"
}

func (y *Yoovi) Balance() int {
	var result YooviBalanceResponse

	url := fmt.Sprintf(
		"%s?action=check-balance&api_key=%s&response=json",
		yooviBaseURL,
		y.ApiKey,
	)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		fmt.Println(err)
		return 0
	}

	return result.Balance
}
